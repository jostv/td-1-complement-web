<?php
namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use TheFeed\Controleur\ControleurUtilisateur;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use TheFeed\Lib\Conteneur;

class RouteurURL
{
    public static function traiterRequete() {
        $requete = Request::createFromGlobals();
        $routes = new RouteCollection();

        // Route afficherListe
        $route = new Route("/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
        ]);
        $routes->add("afficherListe", $route);

        // Route creer publications
        $route = new Route("/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::creerDepuisFormulaire",
        ]);
        $route->setMethods("POST");
        $routes->add("creerPublication", $route);

        // Route afficher accueil
        $route = new Route("/", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
        ]);
        $routes->add("afficherAccueil", $route);

        // Route afficherFormulaireConnexion
        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireConnexion",
        ]);
        $route->setMethods("GET");
        $routes->add("afficherFormulaireConnexion", $route);

        // Route connecter
        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::connecter"
        ]);
        $route->setMethods("POST");
        $routes->add("connecter", $route);

        // Route deconnecter
        $route = new Route("/deconnexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::deconnecter"
        ]);
        $routes->add("deconnecter", $route);

        // Route afficher inscription
        $route = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireCreation"
        ]);
        $route->setMethods("GET");
        $routes->add("afficherFormulaireInscription", $route);

        // Route inscription
        $route = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::creerDepuisFormulaire"
        ]);
        $route->setMethods("POST");
        $routes->add("inscrire", $route);

        // Route publications utilisateur
        $route = new Route("/utilisateurs/{idUtilisateur}/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherPublications"
        ]);
        $routes->add("publicationutilisateur", $route);

        $contexteRequete = (new RequestContext())->fromRequest($requete);

        $associateurUrl = new UrlMatcher($routes, $contexteRequete);
        $donneesRoute = $associateurUrl->match($requete->getPathInfo());

        $requete->attributes->add($donneesRoute);

        $resolveurDeControleur = new ControllerResolver();
        $controleur = $resolveurDeControleur->getController($requete);

        $resolveurDArguments = new ArgumentResolver();
        $arguments = $resolveurDArguments->getArguments($requete, $controleur);

        $generateurUrl = new UrlGenerator($routes, $contexteRequete);
        $assistantUrl = new UrlHelper(new RequestStack(), $contexteRequete);

        Conteneur::ajouterService("generateurUrl", $generateurUrl);
        Conteneur::ajouterService("assistantUrl", $assistantUrl);


        call_user_func_array($controleur, $arguments);
    }
}