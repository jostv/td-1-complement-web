<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\MessageFlash;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\Conteneur;

class ControleurGenerique {

    protected static function afficherVue(string $cheminVue, array $parametres = []): Response
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        ob_start();
        require __DIR__ . "/../vue/$cheminVue";
        $corpsReponse = ob_get_clean();
        return new Response($corpsReponse);
    }

    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
//    protected static function rediriger(string $controleur = "", string $action = "", array $query = []) : void
//    {
//        $queryString = [];
//        if ($action != "") {
//            $queryString[] = "action=" . rawurlencode($action);
//        }
//        if ($controleur != "") {
//            $queryString[] = "controleur=" . rawurlencode($controleur);
//        }
//        foreach ($query as $name => $value) {
//            $name = rawurlencode($name);
//            $value = rawurlencode($value);
//            $queryString[] = "$name=$value";
//        }
//        $url = "Location: ./controleurFrontal.php?" . join("&", $queryString);
//        header($url);
//        exit();
//    }

    protected static function rediriger(string $route, array $options = []) : void
    {
        /** @var UrlGenerator $generateurUrl */
        $generateurUrl = Conteneur::recupererService("generateurUrl");

        $url = $generateurUrl->generate($route, $options);
        header("Location: $url");
        exit();
    }

    public static function afficherErreur($messageErreur = "", $controleur = ""): void
    {
        $messageErreurVue = "Problème";
        if ($controleur !== "")
            $messageErreurVue .= " avec le contrôleur $controleur";
        if ($messageErreur !== "")
            $messageErreurVue .= " : $messageErreur";

        ControleurGenerique::afficherVue('vueGenerale.php', [
            "pagetitle" => "Problème",
            "cheminVueBody" => "erreur.php",
            "errorMessage" => $messageErreurVue
        ]);
    }

}