<?php

use TheFeed\Lib\ConnexionUtilisateur;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\Conteneur;


/**
 * @var string $pagetitle
 * @var string $cheminVueBody
 * @var String[][] $messagesFlash
 */

/** @var UrlGenerator $generateurUrl */
$generateurUrl = Conteneur::recupererService("generateurUrl");
/** @var UrlHelper $assistantUrl */
$assistantUrl = Conteneur::recupererService("assistantUrl");
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?= $pagetitle ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?= $assistantUrl->getAbsoluteUrl("../ressources/css/styles.css") ?>">
</head>

<body>
    <header>
        <div id="titre" class="center">
<!--            <a href="./publications"><span>The Feed</span></a>-->
            <a href="<?= $generateurUrl->generate("afficherAccueil") ?>"><span>The Feed</span></a>
            <nav>
                <a href="<?= $generateurUrl->generate("afficherAccueil") ?>"><span>Accueil</span></a>
                <?php
                if (!ConnexionUtilisateur::estConnecte()) {
                ?>
<!--                    <a href="./inscription">Inscription</a>-->
                    <a href="<?= $generateurUrl->generate("afficherFormulaireInscription") ?>">Inscription</a>
<!--                    <a href="./connexion">Connexion</a>-->
                    <a href="<?= $generateurUrl->generate("afficherFormulaireConnexion") ?>">Connexion</a>
                <?php
                } else {
                    $idUtilisateurURL = rawurlencode(ConnexionUtilisateur::getIdUtilisateurConnecte());
                ?>
<!--                    <a href="./connexion?action=afficherPublications&controleur=utilisateur&idUtilisateur=--><?php //= $idUtilisateurURL ?><!--">Ma-->
<!--                        page</a>-->
                    <a href="<?= $generateurUrl->generate("publicationutilisateur", ["idUtilisateur" => $idUtilisateurURL]) ?>">Ma
                        page</a>
<!--                    <a href="./deconnexion?action=deconnecter&controleur=utilisateur">Déconnexion</a>-->
                    <a href="<?= $generateurUrl->generate("deconnecter") ?>">Déconnexion</a>
                <?php } ?>
            </nav>
        </div>
    </header>
    <div id="flashes-container">
        <?php
        foreach (["success", "error"] as $type) {
            foreach ($messagesFlash[$type] as $messageFlash) {
        ?>
                <span class="flashes flashes-<?= $type ?>"><?= $messageFlash ?></span>
        <?php
            }
        }
        ?>
    </div>
    <?php
    require __DIR__ . "/$cheminVueBody";
    ?>
</body>

</html>